FROM ubuntu:xenial
MAINTAINER Angelo PACE

RUN apt-get update

# Python
RUN apt-get install -y --force-yes python3-pip wget curl git patch gcc pylint unzip default-jre
RUN mkdir ~/.ssh && ssh-keyscan github.com >> ~/.ssh/known_hosts
RUN pip3 install awscli troposphere boto3 hiera-py nose coverage iptools requests bumpversion inflection pyyaml cerberus gitpython jinja2 argparse

# Sonar Runner
RUN curl http://repo1.maven.org/maven2/org/codehaus/sonar/runner/sonar-runner-dist/2.4/sonar-runner-dist-2.4.zip -o /tmp/sonar-runner-dist-2.4.zip
RUN mkdir /opt/sonar-runner
RUN unzip /tmp/sonar-runner-dist-2.4.zip -d /opt/sonar-runner
RUN chmod 755 -R /opt/sonar-runner

# install RVM, Ruby, and Bundler
RUN apt-get install -y software-properties-common
RUN apt-add-repository ppa:brightbox/ruby-ng
RUN apt-get update
RUN apt-get install -y ruby2.3 ruby2.3-dev
RUN echo 'alias ruby=ruby2.3' >> ~/.bash_aliases
RUN /bin/bash -c "source ~/.bashrc"
RUN gem install bundler
RUN ruby -v && bundler -v

###################################
# Components for Functional Testing

# Install Chrome Dependencies
RUN apt-get install -y libxss1 libappindicator1 libindicator7 libpango1.0-0 fonts-liberation xdg-utils
# Install Chrome
RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
RUN dpkg -i google-chrome*.deb
RUN apt-get install -f
# Install Headless Stuff & Selenium
RUN apt-get install -y xvfb
RUN pip3 install selenium
RUN pip3 install --user pyvirtualdisplay
# Install ChromeDriver
RUN wget -N http://chromedriver.storage.googleapis.com/2.26/chromedriver_linux64.zip
RUN unzip chromedriver_linux64.zip
RUN chmod +x chromedriver
RUN mv -f chromedriver /usr/local/share/chromedriver
RUN ln -s /usr/local/share/chromedriver /usr/local/bin/chromedriver
RUN ln -s /usr/local/share/chromedriver /usr/bin/chromedriver
###################################

# Terraform
RUN wget https://releases.hashicorp.com/terraform/0.8.8/terraform_0.8.8_linux_amd64.zip
RUN unzip terraform_0.8.8_linux_amd64.zip
RUN mv terraform /usr/local/bin/terraform

# Terragrunt
RUN wget https://github.com/gruntwork-io/terragrunt/releases/download/v0.11.0/terragrunt_linux_amd64
RUN mv terragrunt_linux_amd64 /usr/local/bin/terragrunt
RUN chmod +x /usr/local/bin/terragrunt

# Packer
RUN wget https://releases.hashicorp.com/packer/0.12.3/packer_0.12.3_linux_amd64.zip
RUN unzip packer_0.12.3_linux_amd64.zip
RUN mv packer /usr/local/bin/packer
