# Autobots Dockerfiles

## Autobots Amazonia
> The image used in pipelines for amazonia deployments

## Autobots Terraform
> The image used in pipelines for terraform deployments

## Autobots Packer
> The image used in pipelines for packer deployments

## Autobots NatMap & Aremi
> The image used in pipelines for Selenium based testing